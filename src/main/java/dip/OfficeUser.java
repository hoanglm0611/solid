package dip;

import openclosed.ex0.IPost;
import openclosed.ex0.Post;
import single.responsibility.cohension.ex3.Database;

public class OfficeUser {
    private final IPost iPost;

    public OfficeUser(IPost iPost) {
        this.iPost = iPost;
    }

    public void publishNewPost() {
        Database db = new Database();
        String postMessage = "example message";
        iPost.createPost(db, postMessage);
    }

    // Sau này có nhiều loại post chỉ cần tạo class Post mới implement từ interface IPost
    public static void main(String[] args) {
        IPost iPost = new Post();
        OfficeUser officeUser = new OfficeUser(iPost);
        officeUser.publishNewPost();
    }
}
