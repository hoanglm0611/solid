package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public interface IPost {
    public void createPost(Database db, String postMessage);
}
