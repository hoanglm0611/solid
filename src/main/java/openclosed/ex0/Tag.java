package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public class Tag extends Post {
    public void createTag(Database db, String postMessage){
        db.addAsTag(postMessage);
    }
}
