package dry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GoodParent {
    public List<Person> findAllGoodGuy(List<Person> persons){
        List<Person> results = new ArrayList<>();
        for (Person person : persons) {
            if (person.name.startsWith("T")
                    && person.age > 25
                    && person.hairColor == HairColor.BLACK
                    && person.skinColor == SkinColor.WHITE){
                results.add(person);
            }
        }
        return results;
    }

    public List<Person> findAllGoodMan(List<Person> persons) {
        return findAllGoodGuy(persons).stream()
                .filter(person -> person.sex == Gender.MALE)
                .collect(Collectors.toList());
    }

    public List<Person> findAllGoodGirl(List<Person> persons) {
        return findAllGoodGuy(persons).stream()
                .filter(person -> person.sex == Gender.FEMALE)
                .collect(Collectors.toList());
    }
}

class Person {
    String name;
    int age;
    Gender sex;
    HairColor hairColor;
    SkinColor skinColor;


}

enum SkinColor {
    BLACK, BROWN, WHITE
}

enum HairColor {
    RED, BLACK, YELLOW
}

enum Gender {
    FEMALE,
    MALE,
    GAY,
    LES,
}
