package single.responsibility.cohension.ex1;

public class Square extends Shape{

    @Override
    public void draw() {
        System.out.println("draw square now!!");
    }

    @Override
    public void rotate() {
        System.out.println("we rotate this square now!!");
    }

}
