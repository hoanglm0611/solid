package kiss;

public class IntegrationHandlerFactory {

    public IntegrationHandlerFactory() {
    }

    public IntegrationHandler getHandlerFor(String integration) {
        switch (integration){
            case EmailIntegrationHandler.EMAIL:
                return new EmailIntegrationHandler();
            case PushIntegrationHandler.PUSH:
                return new PushIntegrationHandler();
            case SMSIntegrationHandler.SMS:
                return new SMSIntegrationHandler();
            default:
                throw new IllegalArgumentException("No handler found for integration: " + integration);
        }
    }
}