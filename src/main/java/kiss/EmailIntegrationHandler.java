package kiss;

public class EmailIntegrationHandler implements IntegrationHandler {
    public static final String EMAIL = "abc";
    private EmailIntegrationHandler emailHandler;

    @Override
    public IntegrationHandler getHandlerFor(String integration) {
        return emailHandler;
    }
}
