package kiss;

public class PushIntegrationHandler implements IntegrationHandler {
    public static final String PUSH = "glk";
    private PushIntegrationHandler pushHandler;

    @Override
    public IntegrationHandler getHandlerFor(String integration) {
        return pushHandler;
    }
}
